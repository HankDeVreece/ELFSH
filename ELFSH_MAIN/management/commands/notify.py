from ELFSH_MAIN import models
from push_notifications import models as pushmodels
import pytz
import datetime
from django.core.management.base import BaseCommand

# Should be called every 15 minutes or so
from ELFSH_MAIN.util.localization import localize
from ELFSH_MAIN.util.notification import Notification


class Command(BaseCommand):
    help = 'sends notifications'

    def handle(self, *args, **options):
        houses = models.House.objects.all()
        current_time = datetime.datetime.now().astimezone(pytz.UTC)
        current_date = current_time.date()
        for house in houses:
            try:
                models.NotificationProcessed.objects.get(house=house, date=current_date)
                continue
            except models.NotificationProcessed.DoesNotExist:
                # Do not send notifications if already sent
                pass

            models.NotificationProcessed(house=house, date=current_date).save()

            timezone = pytz.timezone(house.timeZone)
            hour, minute = (int(i) for i in house.closingTime.split(":"))
            time = timezone.localize(current_time.replace(hour=hour, minute=minute, second=0, tzinfo=None)
                                     ).astimezone(timezone)
            closing_time = time.astimezone(pytz.UTC)

            if current_time > closing_time:
                print("notifying " + house.name)
                # print("end time: ", closing_time.isoformat())
                # print("cur time: ", current_time.isoformat())
                local_time = datetime.datetime.now().astimezone(timezone)
                local_date = local_time.date()

                eat_states = models.EatState.objects.filter(date=local_date, house=house)
                cook_states = [i for i in eat_states if i.eatState == 2]  # 2 is cooking
                eater_state = [i for i in eat_states if i.eatState != 0]  # Includes eaters and cookers

                if len(cook_states) >= 1:
                    total_eaters = sum(1 + i.extraPeople for i in eat_states if i.eatState != 0)
                    if total_eaters <= 1:
                        continue
                    # TODO: Properly determine language for notification

                    for cooker in cook_states:
                        print("\tnotified " + cooker.user.user.username)
                        message = Notification(
                            localize(None, "push_cook_message", lang=cooker.user.language).format(total_eaters),
                            icon="/static/images/notification_icons/K.png")
                        user = cooker.user
                        devices = pushmodels.WebPushDevice.objects.filter(user=user.user)
                        devices.send_message(message.to_json())

                else:
                    for eater in eater_state:
                        notification = Notification(
                            localize(None, "push_no_message", lang=eater.user.language),

                            icon="/static/images/notification_icons/E.png"
                        )

                        print("\tnotified " + eater.user.user.username)
                        user = eater.user
                        devices = pushmodels.WebPushDevice.objects.filter(user=user.user)
                        devices.send_message(notification.to_json())


if __name__ == "__main__":
    print("Notifying...")
    Command().handle()
