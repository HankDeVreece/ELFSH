from . import *

login_schema = DictSchema(
    {
        "username": StringSchema(regex=r'.+', message="validation_username"),
        "password": StringSchema(regex=r'.+', message="validation_password")
    }
)