from . import *

send_invitation_schema = DictSchema(
    {
        "name": StringSchema(regex=".+", message="validation_name_not_empty"),
        "email": StringSchema(regex=".+@.+", message="validation_email"),
        "lang": StringSchema(regex="en|nl", message="validation_language"),
    }
)

verify_invitation_schema = DictSchema(
    {
        "uuid": StringSchema(regex=".+")
    }
)

register_schema = DictSchema(
    {
        "uuid": StringSchema(regex=".+"),
        "username": StringSchema(regex=".+", message="validation_username"),
        "password": StringSchema(regex=".+", message="validation_password"),
        "password2": StringSchema(regex=".+", message="validation_password")
    }
)

accept_invitation_schema = DictSchema(
    {
        "uuid": StringSchema(regex=".+")
    }
)
