from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseForbidden, JsonResponse
from django.views.decorators.http import require_POST

from ELFSH_MAIN import models
from ELFSH_MAIN.util.localization import localize
from ELFSH_MAIN.util.decorators import login_decorators, validation
from ELFSH_MAIN.schemas import user_schemas

@login_decorators.require_login
def get_current_user_info(request):
    if not request.user.is_authenticated:
        return HttpResponseForbidden("needs authentication")
    user = request.user
    elfsh_user = models.ELFSHUser.objects.get(user=user)
    return JsonResponse({
        "username": user.username,
        "id": elfsh_user.id,
        "email": user.email,
        "is_staff": user.is_staff
    })

@require_POST
@validation.validate(
    user_schemas.login_schema
)
def login_view(request, data):
    user = authenticate(request, username=data.get("username"), password=data.get("password"))
    if user is not None and user.is_active:
        login(request, user=user)
        elfsh_user = models.ELFSHUser.objects.get(user=user)

        return JsonResponse({"status": "success",
                             "user": {
                                 "username": user.username,
                                 "id": elfsh_user.id,
                                 "email": user.email,
                                 "is_staff": user.is_staff
                             }})
    else:
        return JsonResponse({"status": "error", "error": localize(request, "login_invalid"),
                             }, status=400)


@require_POST
def logout_view(request):
    logout(request)
    return JsonResponse({"status": "success"})
