import datetime

from django.http import JsonResponse, HttpResponseBadRequest
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_POST, require_safe

from ELFSH_MAIN import models
from ELFSH_MAIN.util.localization import localize
from ELFSH_MAIN.util.userinfo import get_user_info, get_house_info, get_account_info
from ELFSH_MAIN.views.misc_views import MAX_DAYS
from ELFSH_MAIN.schemas import misc_schema

from ELFSH_MAIN.util.decorators import validation, login_decorators


@require_safe
@login_decorators.require_housemembership
def future_days(request, house):
    eats = models.EatState.objects.filter(house=house, date__gte=datetime.date.today(),
                                          date__lte=datetime.datetime.now() + datetime.timedelta(days=MAX_DAYS))
    user_info = get_user_info(request, house)

    table = [{j["id"]: {"state": 0, "extra": 0} for j in user_info} for i in range(MAX_DAYS)]

    date = datetime.date.today()

    for eat in eats:
        delta_days = (eat.date - datetime.date.today()).days
        table[delta_days][eat.user.user.id] = {"state": eat.eatState, "extra": eat.extraPeople}

    return JsonResponse({"name": house.name,
                         "date": date.isoformat(),
                         "users": user_info,
                         "house_info": get_house_info(request, house),
                         "account_info": get_account_info(request, house),
                         "table": table})


@require_POST
@login_decorators.require_housemembership
@validation.validate(misc_schema.insert_day_schema)
def insert_day(request, house, person, day, month, year, data):
    # Request I'm debugging: POST
    # 	https://elfsh.mousetail.nl/api/update/1/95/3/9/2021
    # Person = 95
    person = get_object_or_404(models.ELFSHUser, user__id=int(person))

    try:
        models.HouseMembership.objects.get(
            user=person,
            house=house
        )
    except models.HouseMembership.DoesNotExist:
        return HttpResponseBadRequest("No house membership exists")

    eat_state = data.get("state", 0)
    eat_extra = data.get("extra", 0)

    date = datetime.date(year=int(year), month=int(month), day=int(day))
    if not datetime.timedelta(days=-1) <= date - datetime.date.today() <= datetime.timedelta(days=15):
        return HttpResponseBadRequest(localize(request, "list_bad_date"))
    try:
        state = models.EatState.objects.get(house=house, user=person, date=date)
    except models.EatState.DoesNotExist:
        state = models.EatState(house=house, user=person, date=date)
    except models.EatState.MultipleObjectsReturned:
        # This should never happen, except if a race condition occurs
        models.EatState.objects.filter(house=house, user=person, date=date).delete()
        state = models.EatState(house=house, user=person, date=date)
    state.eatState = eat_state
    state.extraPeople = eat_extra
    state.save()

    return JsonResponse({"status": "success", "id": state.id, "value": eat_state})
