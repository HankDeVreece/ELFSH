from django.http.response import HttpResponseNotAllowed
from django.shortcuts import render

from ELFSH_MAIN.util.decorators.login_decorators import require_login
from ELFSH_MAIN.models import House, HouseMembership


@require_login
def admin_overview(request):
    if not request.user.is_staff:
        return HttpResponseNotAllowed("You need to be staff")

    houses = House.objects.all()
    members = [
        {"name": house.name,
         "id": house.id,
         "members": [
             {"name": i.user.username,
              "id": i.user.id,
              "first_login": i.user.date_joined,
              "last_login": i.user.last_login} for i in house.users.all()]
         } for house in houses]

    return render(request, "admin_page.html", {"houses": members})
