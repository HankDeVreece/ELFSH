import functools
import json

from django.http import HttpResponseBadRequest

from ELFSH_MAIN.util.localization import try_localize


def validate(schema):
    def _validate(funk):
        @functools.wraps(funk)
        def _validate_inner(request, *args, **kwargs):
            if request.content_type.lower() == "application/json":
                data = json.loads(request.body.decode("utf-8"))
            elif request.method.upper() == "POST":
                data = request.POST
            else:
                data = request.GET

            message = schema.validate(data)
            if message:
                return HttpResponseBadRequest(
                    try_localize(request, message)
                )
            return funk(request, *args, data=data, **kwargs)

        return _validate_inner

    return _validate
