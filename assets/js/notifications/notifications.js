export function registerServiceWorker() {
  window.addEventListener('load',
    () => {
      Promise.resolve().then(() => {
        if ('serviceWorker' in navigator) {
          navigator.serviceWorker.register(
            '/serviceworker.js'
          ).then(() => {
              console.log('service worker registered')
            }
          )
        }
      })
    }
  );
}