import React from 'react';
import { request } from '../../util/request';

export default class LogoutScreen extends React.PureComponent {
  componentDidMount() {
    this.props.onUserChange(false, undefined);
    request(
      '/api/logout', {},
    ).then(
      () => {
        console.log('redirecting...');
        this.props.history.replace(`/${this.props.match.params.lang}/login`);
      },
    ).catch(
      (error) => console.log(error),
    );
  }

  render() {
    return <div>Loading...</div>;
  }
}
