import { localize } from '../../util/localization';
import { request } from '../../util/request';

export function submitExpenseChange(house_id, expense_id, description, amount, paid_by, payed_for, deleted, maxEntries, is_meal) {
  return request(`/api/new/${house_id}`,
    {
      id: expense_id,
      description,
      amount,
      paid_by,
      payed_for,
      deleted,
      max: maxEntries,
      is_meal,
    }).then(
    (data) => {
      if (data.hasOwnProperty('result') && data.hasOwnProperty('result')
        && data.hasOwnProperty('user_info')) {
        return data;
      }

      throw new Error(localize('error_invalid_response'));
    },
  );
}
