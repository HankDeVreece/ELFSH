import React from 'react';

import { request } from '../../util/request';
import { Nav } from '../../components/nav.jsx';
import { localize } from '../../util/localization.js';
import { ErrorView } from '../../components/error.jsx';
import { FullscreenLoading } from '../../components/loading';
import { Expense } from './expense';
import { ExpenseEdit } from './expenseEdit';
import { RefreshDetector } from '../../components/refreshDetector';

export default class Costs extends React.Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.state = {
      loading: true,
      data: undefined,
      costsResponse: undefined,
      error: undefined,
      maxEntries: 6,

      new_field: {
        amount: 0,
        description: '',
        paid_by: undefined, // this.props.user.id,
        payed_for: {},
        is_meal: false,
      },
    };
  }

  componentDidMount() {
    document.title = `${localize('page_costs')} - ELFSH`;
    this.reload(this.state.maxEntries);
  }

  reload(max) {
    max = max || this.state.maxEntries;
    request(`/api/costs/${this.props.match.params.house_id}/`,
      { max }).then(
      (data) => {
        const payed_for = {};
        data.user_info.map(
          (i) => {
            payed_for[i.name] = 0;
          },
        );

        this.setState(
          {
            data,
            loading: false,
            new_field: {
              ...this.state.new_field,
              paid_by: this.state.new_field.paid_by || data.account_info.id,
              payed_for: { ...this.state.new_field.payed_for, ...payed_for },

            },
          },
        );
      },
    ).catch((error) => {
      console.log(error);
      this.setState(
        {
          loading: false,
          error,
        },
      );
    });
  }

  refresh() {
    this.reload(this.state.maxEntries);
  }

  render() {
    return (
      <>
        <Nav
          match={this.props.match}
          selected="cost"
        />
        <RefreshDetector onRefresh={this.refresh.bind(this)} />
        {
        this.state.loading ? (
          <FullscreenLoading />
        )
          : this.state.error
            ? <ErrorView error={this.state.error} />
            : (
              <>
                <div className="expense-container">
                  {
                this.state.data.result.map(
                  (i) => (
                    // this is the ugliest nested function ever
                    <Expense
                      key={i.id}
                      expense={i}
                      user_info={this.state.data.user_info}
                      house_id={this.props.match.params.house_id}
                      replaceData={(data) => this.setState({ data })}
                      maxEntries={this.state.maxEntries}
                      onChange={(property, value) => {
                        this.setState(
                          (state) => {
                            const { data } = state;
                            data.result.map(
                              (item) => {
                                if (item.id === i.id) {
                                  item[property] = value;
                                }
                                return item;
                              },
                            );
                            return { data };
                          },
                        );
                      }}
                    />
                  ),
                )
              }
                </div>
                {
              this.state.maxEntries === this.state.data.result.length
                ? (
                  <button onClick={() => {
                    const entries = this.state.maxEntries + 12;
                    this.setState({ maxEntries: entries },
                      this.reload(entries));
                  }}
                  >
                    Load more
                  </button>
                ) : null
            }
                <ExpenseEdit
                  user_info={this.state.data.user_info}
                  params={this.state.new_field}
                  house_id={this.props.match.params.house_id}
                  account_info={this.state.data.account_info}
                  onChange={(name, value) => this.setState((state) => (
                    { new_field: { ...state.new_field, [name]: value } }))}
                  maxEntries={this.state.maxEntries}
                  replaceData={(data) => this.setState({ data })}
                />
              </>
            )
      }
      </>
    );
  }
}
