import React from 'react';
import { Link } from 'react-router-dom';
import { localize } from '../../util/localization';
import { request } from '../../util/request';
import { ErrorView } from '../../components/error';

export default class RequestResetPage extends React.Component {
  constructor() {
    super();

    this.state = {
      email: '',
      error: '',
      done: false,
      loading: false,
    };
  }

  componentDidMount() {
    document.title = `${localize('page_reset')} - ELFSH`;
  }

  submit(event) {
    event.preventDefault();
    this.setState(
      { loading: true },
    );

    request(
      '/api/reset/request',
      {
        email: this.state.email,
      },
    ).then(
      () => this.setState({ done: true, loading: false }),
    ).catch(
      (error) => this.setState({ error, loading: false }),
    );
  }

  render() {
    if (this.state.done) {
      return <div className="done">Done!</div>;
    }

    return (
      <form>
        <ErrorView error={this.state.error} />
        <h1>{localize('reset_title')}</h1>
        <div className="text-container">
          {localize('reset_explain')}
        </div>
        <label className="inputLabel">
          Email:
          <input
            type="email"
            value={this.state.email}
            onChange={(event) => this.setState({ email: event.target.value })}
            disabled={this.state.loading}
          />
        </label>

        <button onClick={this.submit.bind(this)}>{localize('cost_submit')}</button>
        <br />
        <Link to={`/${this.props.match.params.lang}/login`}>{localize('back')}</Link>
      </form>
    );
  }
}
