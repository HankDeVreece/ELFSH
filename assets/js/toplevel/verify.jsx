import React from 'react';
import { request } from '../util/request';
import { ErrorView } from '../components/error';
import { localize } from '../util/localization';

export default class VerifyScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      error: false,
    };
  }

  componentDidMount() {
    document.title = `${localize('page_verify')} - ELFSH`;

    request(
      `/api/verify/${this.props.match.params.code}`,
      {},
    ).then(
      (data) => {
        this.props.onUserChange(
          true,
          data.user,
        );
        this.props.history.replace(
          `/${this.props.match.params.lang}/settings/${data.house_id}`,
        );
      },
    ).catch(
      (error) => this.setState({
        error,
        loading: false,
      }),
    );
  }

  render() {
    return (
      <div>
        {
        this.state.loading ? <h1>Loading...</h1> : null
      }
        {
        this.state.error ? <ErrorView error={this.state.error} /> : null
      }
      </div>
    );
  }
}
