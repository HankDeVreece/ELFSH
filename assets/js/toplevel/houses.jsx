import React from 'react';
import { Link } from 'react-router-dom';
import { localize } from '../util/localization';
import { FullscreenLoading } from '../components/loading';
import { ErrorView } from '../components/error';

export default class HousesScreen extends React.Component {
  constructor() {
    super();

    this.state = { loading: true, error: undefined };
  }

  componentDidMount() {
    document.title = `${localize('page_houses')} - ELFSH`;

    this.setState(
      { loading: true },
    );
    fetch('/api/houses/',
      {
        credentials: 'same-origin',
      }).then(
      (request) => {
        if (!request.ok) {
          if (request.status === 401) {
            throw new Error('not logged in');
          } else {
            throw new Error(request.statusText);
          }
        }
        return request;
      },
    ).then(
      (r) => r.json(),
    ).then(
      (data) => {
        if (data.houses.length === 1) {
          console.log(this.props.match);
          this.props.history.push(`/${this.props.match.params.lang}/list/${data.houses[0].id}`);
        } else {
          this.setState({
            loading: false,
            houses: data.houses,
          });
        }
      },
    )
      .catch(
        (error) => {
          this.setState({ error });
        // alert(error);
        },
      );
  }

  render() {
    if (this.state.error) {
      return <ErrorView error={this.state.error} />;
    }
    if (this.state.loading) {
      return (
        <>
          <FullscreenLoading />
          <p>Houses</p>
        </>
      );
    }

    return (
      <>
        {this.state.houses.map(
          (house) => <Link to={`/${this.props.match.params.lang}/list/${house.id}`}>house.name</Link>,
        )}
      </>
    );

    // Todo: implement this
  }
}
