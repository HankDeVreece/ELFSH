// eslint-disable-next-line import/prefer-default-export
export function objectMap(object, mapFn) {
  return Object.keys(object)
    .reduce((result, key) => {
      // eslint-disable-next-line no-param-reassign
      result[key] = mapFn(object[key]);
      return result;
    }, {});
}
