import React from 'react';

export function ValidationMessage({ error }) {
  if (error) {
    return <div className="errorMessage">{error}</div>;
  }
  return null;
}
