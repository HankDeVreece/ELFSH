import React from 'react';

export class RefreshDetector extends React.Component {
  constructor() {
    super();
    this.onRefresh = () => {
      this.props.onRefresh();
    };
  }

  componentDidMount() {
    window.addEventListener('focus', this.onRefresh);
  }

  componentWillUnmount() {
    window.removeEventListener('focus', this.onRefresh);
  }

  render() {
    return null;
  }
}
