import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome/index.es';
import { localize } from '../util/localization';

export class FullscreenLoading extends React.Component {
  render() {
    return (
      <>
        <FontAwesomeIcon icon="circle-notch" spin />
        {localize('loading')}
      </>
    );
  }
}

export const InlineLoading = FullscreenLoading;
