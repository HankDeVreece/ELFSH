import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { localize } from '../util/localization';

export function Footer() {
  return (
    <div className="footer">
      {localize('footer_line_1')}
      <br/>
      {localize('footer_line_2')}
      {BUILT_AT}
      {' '}
      "{COMMIT_MESSAGE}"<br/>
      {localize('footer_line_3')}
      <a href="https://gitlab.com/mousetail/ELFSH">
        <FontAwesomeIcon icon={['fab', 'gitlab']}/>
        {' '}
        gitlab
      </a>
    </div>
  );
}
