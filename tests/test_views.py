import datetime

from django.test import TestCase
from django.contrib.auth import models as authmodels

from ELFSH_MAIN import models
from django.db import transaction


# Creates a scenario with a a few users
class BasicTestCase(TestCase):
    def createUser(self, username, password, email):
        user = authmodels.User.objects.create_user(
            username=username,
            password=password,
            email=email
        )
        elfsh_user = models.ELFSHUser(
            user=user
        )
        return elfsh_user


    def setUp(self):
        super().setUp()

        self.houses = [models.House(
            name="Test house " + str(i)
        ) for i in range(3)]
        for house in self.houses:
            house.save()

        print("set up")

        self.house = self.houses[1]

        self.users = [self.createUser("user_" + str(i), "pass_" + str(i),
                                      "user_" + str(i) + "@provider.nl")
                      for i in range(6)]

        self.passwords = ["pass_" + str(i) for i in range(6)]
        for user in self.users:
            user.save()
            membership = models.HouseMembership(user=user, house=self.house)
            membership.save()

        print(self.users[4].user.email)
        print([(i.username, i.email) for i in authmodels.User.objects.all().filter(email=self.users[4].user.email)])

        self.client.login(username=self.users[0].user.username,
                          password=self.passwords[0])


class TableFunctionTest(TestCase):
    def setUp(self):
        user = authmodels.User.objects.create_user(
            "test_username",
            password="test_password"
        )

        elfsh_user = models.ELFSHUser(user=user,
                                      )
        elfsh_user.save()

        house = models.House(
            name="Test House"
        )

        house.save()
        self.house_id = house.id

        membership = models.HouseMembership(
            user=elfsh_user,
            house=house
        )
        membership.save()

        self.client.get("/")

        self.client.login(username="test_username", password="test_password")

    def testBasis(self):
        response = self.client.get("/api/future_days/" + str(self.house_id))
        self.assertEqual(response.status_code, 200)


class ConvertCookingTest(BasicTestCase):
    def testOneEntity(self):
        eatStates = models.EatState.objects.filter(date__lte=datetime.date.today(), house=self.house)
        self.assertEquals(len(eatStates), 0)

        state = models.EatState(eatState=2, date=datetime.date(day=1, month=1, year=1970),
                                house=self.house, user=self.users[3])
        state.save()

        eatStates = models.EatState.objects.filter(date__lte=datetime.date.today(), house=self.house)
        self.assertGreater(len(eatStates), 0)

        response = self.client.get("/api/costs/" + str(self.house.id))
        self.assertEquals(response.status_code, 200, response.content)

        eatStates = models.EatState.objects.filter(date__lte=datetime.date.today(), house=self.house)
        self.assertEquals(len(eatStates), 0)

        expenses = models.Expense.objects.all()
        self.assertEquals(len(expenses), 0)

    def testTwoTest(self):
        for user in self.users[1:]:
            state = models.EatState(eatState=1, date=datetime.date(day=1, month=1, year=1970),
                                    house=self.house, user=user)
            state.save()
        state = models.EatState(eatState=2, date=datetime.date(day=1, month=1, year=1970),
                                house=self.house, user=self.users[0])
        state.save()

        response = self.client.get("/api/costs/" + str(self.house.id))
        self.assertEquals(response.status_code, 200, response.content)

        eatStates = models.EatState.objects.filter(date__lte=datetime.date.today(), house=self.house)
        self.assertEquals(len(eatStates), 0)

        expenses = models.Expense.objects.all()
        self.assertEquals(len(expenses), 1)

        expense = expenses[0]
        self.assertEquals(expense.date, datetime.date(day=1, month=1, year=1970))
        self.assertEquals(expense.is_meal, True)

        payed_for_users = list(expense.payed_for.all())
        # Make sure the users are both subsets of eachother
        for i in self.users:
            self.assertIn(i, payed_for_users)
        for i in payed_for_users:
            self.assertIn(i, self.users)

        houseMembers = models.HouseMembership.objects.filter(house=self.house, active=True)
        for i in houseMembers:
            self.assertNotEqual(i.points, 0)

    def testExtraPeople(self):
        for i, user in enumerate(self.users):
            state = models.EatState(eatState=2 if i == 1 else 1, date=datetime.date(day=1, month=1, year=1970),
                                    house=self.house, user=user, extraPeople=1 if i == 0 else 0)
            state.save()

        response = self.client.get("/api/costs/" + str(self.house.id))
        self.assertEquals(response.status_code, 200, response.content)

        expense = models.Expense.objects.all()[0]

        expense_user = models.ExpensePayedFor.objects.get(expense=expense, extra=1)
        self.assertEquals(expense_user.user.id, self.users[0].id)

        expense.cost = 10 * (len(self.users) + 1)

        costs = expense.get_costs_per_person()
        for cost in costs:
            if cost["id"] == self.users[1].id:
                self.assertEquals(cost["points"], len(self.users))
                self.assertEquals(cost["cost"], 10 * (len(self.users)))
            elif cost["id"] == self.users[0].id:
                self.assertEquals(cost["points"], -2),
                self.assertEquals(cost["cost"], -20)
            else:
                self.assertEquals(cost["points"], -1)
                self.assertEquals(cost["cost"], -10)

    def testTwoDaysAtOnce(self):
        for day in range(0, 3):
            date = datetime.date.today() - datetime.timedelta(days=day + 1)

            for i, user in enumerate(self.users):
                es = models.EatState(user=user, eatState=2 if day == i else 1, date=date, house=self.house)
                es.save()

        response = self.client.get("/api/costs/" + str(self.house.id))
        self.assertEquals(response.status_code, 200, response.content)

        expenses = models.Expense.objects.all()

        self.assertEquals(len(expenses), 3)
