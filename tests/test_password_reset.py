from .test_views import BasicTestCase
import json

from django.core import mail


class PasswordResetTests(BasicTestCase):
    def testPasswordResetBadKey(self):
        self.client.logout()

        response = self.client.get(
            "/api/reset/verify/1234566fgg"
        )  # Include letter higher than F

        self.assertIn(response.status_code, (400, 404))

    def testPasswordReset1(self):
        self.client.logout()

        response = self.client.post(
            "/api/reset/request",
            json.dumps({"email": self.users[0].user.email}),
            content_type="application/json"
        )

        self.assertEquals(response.status_code, 200)

        self.assertEquals(len(mail.outbox), 1)

        email = mail.outbox[0].body.split("\n")
        link = max(email, key=lambda i: i.count("/"))

        key = link.split("/")[-1]

        response = self.client.get(
            "/api/reset/verify/" + key
        )

        self.assertEquals(response.status_code, 200, response.content.decode("utf-8"))

        response = self.client.post(
            "/api/reset/apply/" + key,
            json.dumps(
                {
                    "password": "password"
                }
            ),
            content_type="application/json"
        )

        self.assertEquals(response.status_code, 400)

        response = self.client.post(
            "/api/reset/apply/" + key,
            json.dumps(
                {
                    "password": "1sd-j8&5dk*x84dJ*"
                }
            ),
            content_type="application/json"
        )

        self.assertEquals(response.status_code, 200)

        response = self.client.get(
            "/api/reset/verify/" + key
        )

        self.assertEquals(response.status_code, 400)

        response = self.client.post(
            "/api/login",
            json.dumps(
                {
                    "username": self.users[0].user.username,
                    "password": "1sd-j8&5dk*x84dJ*"
                }
            ),
            content_type="application/json"
        )
        self.assertEquals(response.status_code, 200)
