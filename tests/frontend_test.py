from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.core.servers.basehttp import WSGIServer
from django.test.testcases import LiveServerThread, WSGIRequestHandler

from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from .test_views import BasicTestCase
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from ELFSH_MAIN import models


class LiveServerSingleThread(LiveServerThread):
    def _create_server(self):
        return WSGIServer((self.host, self.port), WSGIRequestHandler, allow_reuse_address=False)


class FrontendTest(StaticLiveServerTestCase):
    fixtures = ['initial.json']

    def setUp(self):
        super().setUp()

        print("set up")
        self.house = models.House.objects.get(id=7)
        self.users = [i.user for i in self.house.housemembership_set.all()]

        self.passwords = ["password_" + str(i) for i in range(len(self.users))]
        self.client.logout()
        self.browser.get(self.live_server_url)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        options = FirefoxOptions()
        options.headless = True
        cls.browser = webdriver.Firefox(
            firefox_options=options
        )
        cls.browser.implicitly_wait(10)

    def waitForElement(self, selector):
        return WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, selector))
        )

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super().tearDownClass()

    # server_thread_class = LiveServerSingleThread
