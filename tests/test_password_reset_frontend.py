from .frontend_test import FrontendTest
from django.core import mail
import time


class PasswordResetTest(FrontendTest):
    def test_password_reset(self):
        self.client.logout()

        self.browser.get(self.live_server_url + "/en/reset")
        elem = self.waitForElement("input[type=email]")
        elem.send_keys(self.users[0].user.email)
        self.waitForElement("button").click()

        self.waitForElement('.done')
        self.assertEquals(len(mail.outbox), 1)

        email = mail.outbox[0].body.split("\n")
        link = max(email, key=lambda i: i.count("/"))
        key = link.split("/")[-1]

        self.browser.get(self.live_server_url + '/en/reset/' + key)
        password1 = self.waitForElement('label:first-of-type input[type=password]')
        password2 = self.waitForElement('label:last-of-type input[type=password]')

        password1.send_keys('1cj5$%jBC34')
        password2.send_keys('1cj5$%jBC34')

        self.waitForElement('button').click()

        username = self.waitForElement('#username')
        password = self.waitForElement('#password')
        username.send_keys(self.users[0].user.username)
        password.send_keys('1cj5$%jBC34')

        self.waitForElement('button[type=submit]').click()

        self.waitForElement('table')
