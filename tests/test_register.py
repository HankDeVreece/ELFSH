from django.test import TestCase

import json

from .test_views import BasicTestCase
from django.core import mail

from django.contrib.auth import models as authmodels

from ELFSH_MAIN import models


class TestRegister(BasicTestCase):
    def verityLogin(self):
        pass

    def testSendEmail(self):
        email_adress = "Albert@albert.nl"

        result = self.client.post("/api/send_invitation/" + str(self.house.id),
                                  {"name": "Albert",
                                   "email": email_adress,
                                   "lang": "en"}
                                  )
        self.assertEquals(result.status_code, 200, result.content.decode("utf-8"))

        self.assertEquals(len(mail.outbox), 1)
        email = mail.outbox[0]

        link = max(email.body.split("\n"), key=lambda i: i.count("/"))
        key = max(link.split("/"), key=len)

        invitation = models.Invitation.objects.get(id=key)
        self.assertEquals(email.to[0], email_adress)
        self.assertNotEquals(email_adress, invitation.email)
        self.assertEquals(email_adress.lower(), invitation.email)

    def testFlow1en5(self):
        """
        Flow 1:

        Checks that after a invitation when the user is currently logged in as the first users invitation.
        :return:
        """
        invitation = models.Invitation(
            email=self.users[0].user.email,
            house=self.house
        )
        invitation.save()

        house_membership = models.HouseMembership.objects.get(
            user=self.users[0]
        )
        house_membership.delete()

        verify = self.client.post(
            "/api/verify_invitation",
            {
                "uuid": invitation.id
            }
        )
        self.assertEquals(verify.status_code, 200,
                          verify.content)
        data = json.loads(
            verify.content
        )
        self.assertTrue(data["user_exists"])
        self.assertTrue(data["user_loggedin"])
        self.assertTrue(data["user_same"])

        response = self.client.post("/api/accept_invitation",
                                    {
                                        "uuid": str(invitation.id)
                                    })
        self.assertEquals(response.status_code, 200)

        models.HouseMembership.objects.get(
            house=self.house,
            user=self.users[0]
        )

    def testFlow2and3(self):
        """
        Test user exists, but not logged in
        :return:
        """
        response = self.client.post("/api/logout")
        self.assertEqual(response.status_code, 200)

        invitation = models.Invitation(
            email=self.users[0].user.email,
            house=self.house
        )
        invitation.save()

        membership = models.HouseMembership.objects.get(
            house=self.house,
            user=self.users[1]
        )
        membership.delete()

        response = self.client.post("/api/verify_invitation",
                                    {
                                        "uuid": invitation.id
                                    })
        self.assertEquals(response.status_code, 200)

        data = json.loads(response.content)
        self.assertTrue(data["user_exists"])
        self.assertFalse(data["user_loggedin"])
        self.assertFalse(data["user_same"])

        response = self.client.post(
            "/api/login",
            {
                "username": self.users[1].user.username,
                "password": self.passwords[1]
            }
        )
        self.assertEqual(
            response.status_code, 200
        )

        response = self.client.post(
            "/api/accept_invitation",
            {
                "uuid": str(invitation.id)
            }
        )
        self.assertNotEquals(response.status_code, 200, response.content)

    def testFlow3(self):
        """
        Test different user logged in
        :return:
        """
        invitation = models.Invitation(
            email=self.users[2].user.email,
            house=self.house
        )
        invitation.save()

        response = self.client.post(
            "/api/verify_invitation",
            {
                "uuid": invitation.id
            }
        )
        self.assertEquals(response.status_code, 200)

        data = json.loads(response.content)
        self.assertTrue(data["user_exists"])
        self.assertTrue(data["user_loggedin"])
        self.assertFalse(data["user_same"])

    def testFlow4(self):
        """
        Test when email does not yet exist, so creating a new account
        :return:
        """
        correct_username = "1q2w3"
        correct_password = "sdK=?j-34.IO#uF8s$ku"

        self.client.logout()

        invitation = models.Invitation(
            email="differentemail@provider.nl",
            house=self.house
        )
        invitation.save()

        response = self.client.post(
            "/api/verify_invitation",
            {
                "uuid": invitation.id
            }
        )
        self.assertEquals(response.status_code, 200)

        data = json.loads(response.content)
        self.assertFalse(data["user_exists"])
        self.assertFalse(data["user_loggedin"])
        self.assertFalse(data["user_same"])

        # test failure 1
        response = self.client.post(
            "/api/create_account",
            {
                "username": self.users[0].user.username,
                "password": correct_password,
                "password2": correct_password,
                "uuid": invitation.id
            }
        )
        self.assertNotEqual(response.status_code, 200)

        # test failure 2
        response = self.client.post(
            "/api/create_account",
            {
                "username": correct_username,
                "password": correct_username,
                "password2": correct_username,
                "uuid": invitation.id
            }
        )
        self.assertNotEqual(response.status_code, 200)

        with self.assertRaises(authmodels.User.DoesNotExist):
            authmodels.User.objects.get(username=correct_username)

        # test success
        response = self.client.post(
            "/api/create_account",
            {
                "username": correct_username,
                "password": correct_password,
                "password2": correct_password,
                "uuid": invitation.id
            }
        )
        self.assertEqual(response.status_code, 200)

        # Make user the user exists
        authmodels.User.objects.get(username=correct_username)

        response = self.client.post(
            "/api/login",
            {
                "username": correct_username,
                "password": correct_password
            }
        )
        self.assertEquals(response.status_code, 200)
